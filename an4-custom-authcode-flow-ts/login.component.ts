
import {Component, OnInit} from '@angular/core';

import {UserDto} from './user.dto';
import {OauthService} from '../oauth/oauth.service';
import {ActivatedRoute, Params} from '@angular/router';


@Component ({
  selector: 'app-login',
  templateUrl: './login.component.html'
})
export class LoginComponent implements  OnInit {

  userDTO: UserDto;

  constructor(private oauthService: OauthService, private activatedRoute: ActivatedRoute) {}

  public ngOnInit () {
    // subscribe to router event
    this.activatedRoute.queryParams.subscribe((params: Params) => {
      this.oauthService.login(params['code'])
        .subscribe((user) => this.userDTO = user);
    });
  }

  public isAuthenticated () {
    return this.oauthService.isAuthenticated;
  }

  public logout () {
    this.oauthService.logout();
  }
}
