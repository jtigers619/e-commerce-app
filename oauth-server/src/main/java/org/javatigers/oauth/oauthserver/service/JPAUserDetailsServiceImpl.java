package org.javatigers.oauth.oauthserver.service;

import org.javatigers.oauth.oauthserver.repository.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 * 
 * @author ad
 *
 */
@Service
public class JPAUserDetailsServiceImpl implements UserDetailsService {
	
	private final AccountRepository accountRepository;
	
	@Autowired
	public JPAUserDetailsServiceImpl (AccountRepository accountRepository) {
		this.accountRepository = accountRepository;
	}
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		return accountRepository.findByUsername(username)
				.map(acc -> new User(acc.getUsername()
						, acc.getPassword()
						, acc.isActive()
						, acc.isActive()
						, acc.isActive()
						, acc.isActive()
						, AuthorityUtils.createAuthorityList("ROLE_ADMIN, ROLE_USER")))
				.orElseThrow(() -> new UsernameNotFoundException("no user named " + username + "!"));
	}

}
