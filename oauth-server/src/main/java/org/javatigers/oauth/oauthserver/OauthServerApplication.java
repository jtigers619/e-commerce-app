package org.javatigers.oauth.oauthserver;

import java.util.stream.Stream;

import org.javatigers.oauth.oauthserver.model.Account;
import org.javatigers.oauth.oauthserver.repository.AccountRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.stereotype.Component;

@EnableDiscoveryClient
@EnableResourceServer
@SpringBootApplication
public class OauthServerApplication {
	
	public static void main(String[] args) {
		SpringApplication.run(OauthServerApplication.class, args);
	}
	
}

@Component
class AccountsCLR implements CommandLineRunner {

	private final AccountRepository accountRepository;
	
	public AccountsCLR (AccountRepository accountRepository) {
		this.accountRepository = accountRepository;
	}
	
	@Override
	public void run(String... args) throws Exception {
		Stream.<String>of ("ad,spring", "dd,ang")
		.map(str -> str.split(","))
		.map(tuple -> Account.builder()
								.username(tuple[0])
								.password(tuple[1])
								.active(true)
							.build())
		.forEach (accountRepository::save); 
		
		accountRepository.findAll()
			.forEach(System.out::println);
	}
	
}
