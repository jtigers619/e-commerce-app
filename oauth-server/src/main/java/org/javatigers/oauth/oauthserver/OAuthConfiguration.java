package org.javatigers.oauth.oauthserver;

import java.security.KeyPair;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.rsa.crypto.KeyStoreKeyFactory;

/**
 * 
 * @author ad
 *
 */
@Configuration
@EnableAuthorizationServer
public class OAuthConfiguration extends AuthorizationServerConfigurerAdapter {
	
	private final AuthenticationManager authenticationManager;
	private @Value("${keystore.file}") String keyStoreFile;
	private @Value("${keystore.pass}") String keyStorePass;
	private @Value("${keystore.alias}") String keyStoreAlias;
	
	@Autowired
	public OAuthConfiguration (AuthenticationManager authenticationManager) {
		this.authenticationManager = authenticationManager;
	}
	
	@Override
	public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
		clients.inMemory()
			.withClient("acme")
			.secret("acmesecret")
			.authorizedGrantTypes("authorization_code",
					"client_credentials", "password", "implicit", "refresh_token")
			.autoApprove(Boolean.TRUE)
			.scopes("openid")
			.and()
			.withClient("edge")
			.secret("secret")
			.authorizedGrantTypes("authorization_code",
					"client_credentials", "password", "implicit", "refresh_token")
			.autoApprove(Boolean.TRUE)
			.scopes("openid");
	}
	
	@Override
	public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
		endpoints.authenticationManager(authenticationManager)
				 .accessTokenConverter(jwtAccessTokenConverter());
	}
	
	@Override
	public void configure(AuthorizationServerSecurityConfigurer oauthServer) {
		oauthServer.tokenKeyAccess("permitAll()")
			.checkTokenAccess("isAuthenticated()");
	}
	
	@Bean
	public JwtAccessTokenConverter jwtAccessTokenConverter () {
		JwtAccessTokenConverter jwtConverter = new JwtAccessTokenConverter();
		//Keypair configuration
		KeyPair keyPair = new KeyStoreKeyFactory(new ClassPathResource(keyStoreFile), keyStorePass.toCharArray())
				.getKeyPair(keyStoreAlias);
		jwtConverter.setKeyPair(keyPair);
		return jwtConverter;
	}
	
}
