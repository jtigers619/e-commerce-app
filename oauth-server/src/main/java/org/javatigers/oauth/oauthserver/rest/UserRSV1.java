package org.javatigers.oauth.oauthserver.rest;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

import java.security.Principal;

import javax.validation.Valid;

import org.javatigers.oauth.oauthserver.model.Account;
import org.javatigers.oauth.oauthserver.model.UserDTO;
import org.javatigers.oauth.oauthserver.service.AccountService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * 
 * @author ad
 *
 */
@CrossOrigin
@RestController
@RequestMapping(value = "/user")
public class UserRSV1 {

	private final Logger logger = LoggerFactory.getLogger(getClass());
	
	private AccountService accountService;
	
	@Autowired
	public UserRSV1(AccountService accountService) {
		this.accountService = accountService;
	}
	
	@RequestMapping(method = GET)
	public UserDTO readPrincipal (Principal principal) {
		return UserDTO
				.builder()
				.username(principal.getName())
				.build();
	}
	
	@RequestMapping(value = "/{accountName}", method = GET)
	public Account readAccount (@PathVariable("accountName") String accountName) {
		return accountService.findByName(accountName)
				.blockingFirst();
	}
	
	@RequestMapping(method = RequestMethod.POST)
	public void createUser(@Valid @RequestBody Account user) {
		accountService.create(user)
			.subscribe(ac -> logger.info("{}",ac), 
					   ex -> logger.info("User not created : {}", ex) , 
					   () -> logger.info("Account created successfully."));
	}
}
