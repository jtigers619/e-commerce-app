package org.javatigers.oauth.oauthserver.service;

import org.javatigers.oauth.oauthserver.model.Account;

import io.reactivex.Observable;

/**
 * 
 * @author ad
 *
 */
public interface AccountService {

	/**
	 * Finds account by given name
	 *
	 * @param accountName
	 * @return found account
	 */
	Observable<Account> findByName(String accountName);

	/**
	 * Checks if account with the same name already exists
	 * Invokes Auth Service user creation
	 * Creates new account with default parameters
	 *
	 * @param user
	 * @return created account
	 */
	Observable<Account> create(Account user);

	/**
	 * Validates and applies incoming account updates
	 * Invokes Statistics Service update
	 *
	 * @param account
	 */
	void update(Account account);
}
