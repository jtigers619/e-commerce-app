package org.javatigers.oauth.oauthserver.repository;
import java.util.Optional;

import org.javatigers.oauth.oauthserver.model.Account;
import org.springframework.data.jpa.repository.JpaRepository;
/**
 * 
 * @author ad
 *
 */
public interface AccountRepository extends JpaRepository<Account, Long>{

	Optional<Account> findByUsername (String username);
}
