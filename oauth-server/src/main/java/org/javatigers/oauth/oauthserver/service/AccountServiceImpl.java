package org.javatigers.oauth.oauthserver.service;

import java.util.Optional;

import org.javatigers.oauth.oauthserver.model.Account;
import org.javatigers.oauth.oauthserver.repository.AccountRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import io.reactivex.Observable;

/**
 * 
 * @author ad
 *
 */
@Service
public class AccountServiceImpl implements AccountService {

	private final Logger logger = LoggerFactory.getLogger(getClass());
	
	private final AccountRepository accountRepository;
	
	@Autowired
	public AccountServiceImpl (AccountRepository accountRepository) {
		this.accountRepository = accountRepository;
	}
	
	@Override
	public Observable<Account> findByName(String accountName) {
		logger.info("find by account name : {}.", accountName);
		return Observable.<String>just (accountName)
				.map(accountRepository::findByUsername)
				.filter(optAcc -> optAcc.isPresent())
				.map(Optional::get);
	}

	@Override
	public Observable<Account> create(Account user) {
		logger.info("Create account : {}.", user);
		return Observable.<Account>create(obSub -> {
			try {
				obSub.onNext(accountRepository.saveAndFlush(user));
				obSub.onComplete();
			} catch (Exception e) {
				obSub.onError(e);
			}
		});
	}

	@Override
	public void update(Account account) {
		logger.info("update account : {}.", account);
		create(account);
	}

}
