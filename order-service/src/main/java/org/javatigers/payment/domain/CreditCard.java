package org.javatigers.payment.domain;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.Month;
import java.time.Year;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonUnwrapped;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Abstraction of credit card.
 * 
 * @author ad
 *
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "credit_cards")
public class CreditCard implements Serializable {

	/**
	 * UUID
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	
	@Version
	@Column(name = "version")
	private Integer version;
	
	@JsonUnwrapped
	private CreditCardNumber number;
	private String cardHolderName;
	private Month expiryMonth;
	private Year expiryYear;
	
	/**
	 * Returns whether the {@link CreditCard} is currently valid.
	 * 
	 * @return
	 */
	@JsonIgnore
	public boolean isValid() {
		return isValid(LocalDate.now());
	}

	/**
	 * Returns whether the {@link CreditCard} is valid for the given date.
	 * 
	 * @param date
	 * @return
	 */
	public boolean isValid(LocalDate date) {
		return date == null ? false : getExpirationDate().isAfter(date);
	}
	
	/**
	 * Returns the {@link LocalDate} the {@link CreditCard} expires.
	 * 
	 * @return will never be {@literal null}.
	 */
	public LocalDate getExpirationDate() {
		return LocalDate.of(expiryYear.getValue(), expiryMonth.getValue(), 1);
	}

	/**
	 * Protected setter to allow binding the expiration date.
	 * 
	 * @param date
	 */
	protected void setExpirationDate(LocalDate date) {
		this.expiryYear = Year.of(date.getYear());
		this.expiryMonth = date.getMonth();
	}
}
