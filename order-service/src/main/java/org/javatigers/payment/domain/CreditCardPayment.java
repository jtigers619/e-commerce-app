package org.javatigers.payment.domain;

import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import org.javatigers.order.domain.Order;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * A {@link Payment} done through a {@link CreditCard}.
 * 
 * @author ad
 */
@Data
@EqualsAndHashCode(callSuper=false)
@NoArgsConstructor
@Entity
public class CreditCardPayment extends Payment {

	/**
	 * UUID
	 */
	private static final long serialVersionUID = 1167474955225455152L;
	
	@ManyToOne
	private CreditCard creditCard;

	/**
	 * Creates a new {@link CreditCardPayment} for the given {@link CreditCard}
	 * and {@link Order}.
	 * 
	 * @param creditCard
	 *            must not be {@literal null}.
	 * @param order
	 */
	public CreditCardPayment(CreditCard creditCard, Order order) {

		super(order);
		Objects.nonNull(creditCard);
		this.creditCard = creditCard;
	}
}
