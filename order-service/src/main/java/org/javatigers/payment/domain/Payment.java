package org.javatigers.payment.domain;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Version;

import org.javatigers.order.domain.Order;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Base class for payment implementations.
 * 
 * @author ad
 *
 */
@Data
@NoArgsConstructor
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public abstract class Payment implements Serializable {

	/**
	 * UUID
	 */
	private static final long serialVersionUID = -1421122532230281890L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	
	@Version
	@Column(name = "id")
	private Integer version;
	
	@JoinColumn(name = "fk_order")
	@OneToOne(cascade = CascadeType.MERGE)
	private Order order;
	
	private LocalDateTime paymentDate;
	
	/**
	 * Creates a new {@link Payment} referring to the given {@link Order}.
	 * 
	 * @param order
	 *            must not be {@literal null}.
	 */
	public Payment(Order order) {

		Objects.nonNull(order);
		this.order = order;
		this.paymentDate = LocalDateTime.now();
	}
	
	/**
	 * Returns a receipt for the {@link Payment}.
	 * 
	 * @return
	 */
	public Receipt getReceipt() {
		return new Receipt(order, paymentDate);
	}

	/**
	 * A receipt for an {@link Order} and a payment date.
	 * 
	 */
	@Data
	@NoArgsConstructor
	@AllArgsConstructor
	public static class Receipt {
		
		private Order order;
		private LocalDateTime date;
	}
}
