package org.javatigers.payment.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Value object to represent a {@link CreditCardNumber}.
 * 
 * @author ad
 *
 */
@Data
@NoArgsConstructor
@Builder
@Embeddable
public class CreditCardNumber implements Serializable {

	/**
	 * UUID
	 */
	private static final long serialVersionUID = 2842397355094592928L;
	private static final String CARD_REG_EX = "[0-9]{16}";
	
	@Column(unique = true)
	private String number;
	
	/**
	 * Creates a new {@link CreditCardNumber}.
	 * 
	 * @param number
	 *            must not be {@literal null} and be a 16 digit numerical only
	 *            String.
	 */
	public CreditCardNumber(String number) {

		if (!isValid(number)) {
			throw new IllegalArgumentException(String.format("Invalid credit card NUMBER %s!", number));
		}

		this.number = number;
	}

	/**
	 * Returns whether the given {@link String} is a valid
	 * {@link CreditCardNumber}.
	 * 
	 * @param number
	 * @return boolean
	 */
	public static boolean isValid(String number) {
		return number == null ? false : number.matches(CARD_REG_EX);
	}
	
}
