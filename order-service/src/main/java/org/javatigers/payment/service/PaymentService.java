package org.javatigers.payment.service;

import org.javatigers.order.domain.Order;
import org.javatigers.payment.domain.CreditCard;
import org.javatigers.payment.domain.CreditCardNumber;
import org.javatigers.payment.domain.CreditCardPayment;
import org.javatigers.payment.domain.Payment;
import org.javatigers.payment.domain.Payment.Receipt;

/**
 * Interface to collect payment services.
 * 
 * @author ad
 */
public interface PaymentService {
	
	/**
	 * Pay the given {@link Order} with the {@link CreditCard} identified by the
	 * given {@link CreditCardNumber}.
	 * 
	 * @param order
	 * @param creditCardNumber
	 * @return CreditCardPayment
	 */
	CreditCardPayment pay(Order order, CreditCardNumber creditCardNumber);

	/**
	 * Returns the {@link Payment} for the given {@link Order}.
	 * 
	 * @param order
	 * @return the {@link Payment} for the given {@link Order} or
	 *         {@literal null} if the Order hasn't been payed yet.
	 */
	Payment getPaymentFor(Order order);

	/**
	 * Takes the receipt
	 * 
	 * @param order
	 * @return Receipt
	 */
	Receipt takeReceiptFor(Order order);
}
