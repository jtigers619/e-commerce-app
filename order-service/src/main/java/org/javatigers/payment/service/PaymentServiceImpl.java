package org.javatigers.payment.service;

import org.javatigers.order.domain.Order;
import org.javatigers.order.domain.Order.Status;
import org.javatigers.order.repository.OrderRepository;
import org.javatigers.payment.domain.CreditCard;
import org.javatigers.payment.domain.CreditCardNumber;
import org.javatigers.payment.domain.CreditCardPayment;
import org.javatigers.payment.domain.Payment;
import org.javatigers.payment.domain.Payment.Receipt;
import org.javatigers.payment.exception.PaymentException;
import org.javatigers.payment.repository.CreditCardRepository;
import org.javatigers.payment.repository.PaymentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Implementation of {@link PaymentService} delegating persistence operations to
 * {@link PaymentRepository} and {@link CreditCardRepository}.
 * 
 * @author ad
 */
@Service
@Transactional
public class PaymentServiceImpl implements PaymentService {

	private final CreditCardRepository creditCardRepository;
	private final PaymentRepository paymentRepository;
	private final OrderRepository orderRepository;
	
	/**
	 * Intialization
	 * 
	 * @param creditCardRepository
	 * @param paymentRepository
	 * @param orderRepository
	 */
	@Autowired
	public PaymentServiceImpl(CreditCardRepository creditCardRepository, PaymentRepository paymentRepository, OrderRepository orderRepository) {
		this.creditCardRepository = creditCardRepository;
		this.paymentRepository = paymentRepository;
		this.orderRepository = orderRepository;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public CreditCardPayment pay(Order order, CreditCardNumber creditCardNumber) {
		if (order.isPaid()) {
			throw new PaymentException(order, "Order already paid!");
		}

		CreditCard creditCard = creditCardRepository.findByNumber(creditCardNumber);

		if (creditCard == null) {
			throw new PaymentException(order,
					String.format("No credit card found for number: %s", creditCardNumber.getNumber()));
		}

		if (!creditCard.isValid()) {
			throw new PaymentException(order, String.format("Invalid credit card with number %s, expired %s!",
					creditCardNumber.getNumber(), creditCard.getExpirationDate()));
		}

		order.markPaid();
		return paymentRepository.save(new CreditCardPayment(creditCard, order));
	}

	/**
	 * {@inheritDoc}
	 */
	@Transactional(readOnly = true)
	@Override
	public Payment getPaymentFor(Order order) {
		return paymentRepository.findByOrder(order);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Receipt takeReceiptFor(Order order) {
		order.setStatus(Status.TAKEN);
		orderRepository.save(order);

		Payment payment = getPaymentFor(order);
		return payment == null ? null : payment.getReceipt();
	}

}
