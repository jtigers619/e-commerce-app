package org.javatigers.payment.event;

import org.springframework.context.ApplicationEvent;

import lombok.Getter;

/**
 * {@link ApplicationEvent} to be thrown when an {@link Order} has been payed.
 * 
 * @author ad
 */
public class OrderPaidEvent extends ApplicationEvent {
	
	/**
	 * UUID
	 */
	private static final long serialVersionUID = 8645264698716561502L;
	
	@Getter
	private final long orderId;
	
	/**
	 * Creates a new {@link OrderPaidEvent}
	 * 
	 * @param orderId
	 *            the id of the order that just has been payed
	 * @param source
	 *            must not be {@literal null}.
	 */
	public OrderPaidEvent(long orderId, Object source) {

		super(source);
		this.orderId = orderId;
	}

}
