package org.javatigers.payment.event;

import org.aspectj.lang.annotation.AfterReturning;
import org.javatigers.order.domain.Order;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationEventPublisherAware;
import org.springframework.core.Ordered;

/**
 * Aspect to publish an {@link OrderPaidEvent} on successful execution of
 * {@link PaymentService#pay(Order, CreditCardNumber)} <em>after</em> the
 * transaction has completed. Manually defines the order of the aspect to be
 * <em>before</em> the transaction aspect.
 * 
 */
public class PaymentAspect implements ApplicationEventPublisherAware, Ordered {

	private ApplicationEventPublisher applicationEventPublisher;
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getOrder() {
		return Ordered.HIGHEST_PRECEDENCE - 10;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setApplicationEventPublisher(ApplicationEventPublisher applicationEventPublisher) {
		this.applicationEventPublisher = applicationEventPublisher;
	}

	/**
	 * Publishes an {@link OrderPaidEvent} for the given {@link Order}.
	 * 
	 * @param order
	 */
	@AfterReturning(value = "execution(* org.javatigers.payment.service.PaymentService.pay(org.javatigers.order.domain.Order, ..)) && args(order, ..)")
	public void triggerPaymentEvent(Order order) {
		if (order == null) {
			return;
		}
		this.applicationEventPublisher.publishEvent(new OrderPaidEvent(order.getId(), this));
	}
}
