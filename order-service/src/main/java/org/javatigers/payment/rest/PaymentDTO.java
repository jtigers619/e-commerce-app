package org.javatigers.payment.rest;

import org.javatigers.order.domain.MonetaryAmount;
import org.javatigers.payment.domain.CreditCard;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class PaymentDTO {
	private MonetaryAmount amount;
	private CreditCard creditCard;
}
