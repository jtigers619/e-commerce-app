package org.javatigers.payment.rest;

import static org.springframework.web.bind.annotation.RequestMethod.DELETE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.PUT;

import org.javatigers.order.domain.Order;
import org.javatigers.order.repository.OrderRepository;
import org.javatigers.order.rest.OrderRSV1;
import org.javatigers.payment.domain.CreditCardNumber;
import org.javatigers.payment.domain.CreditCardPayment;
import org.javatigers.payment.domain.Payment;
import org.javatigers.payment.domain.Payment.Receipt;
import org.javatigers.payment.service.PaymentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.support.DomainClassConverter;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Spring rest controller to handle payments for an {@link Order}.
 *
 * @author ad
 */
@CrossOrigin
@RestController
@RequestMapping("/api/v1/")
public class PaymentRSV1 {

	private static final Logger logger = LoggerFactory.getLogger(OrderRSV1.class);
	private final PaymentService paymentService;
	private final OrderRepository orderRepository;
	
	@Autowired
	public PaymentRSV1(PaymentService paymentService, OrderRepository orderRepository) {
		this.paymentService = paymentService;
		this.orderRepository = orderRepository;
	}
	
	/**
	 * Accepts a payment for an {@link Order}
	 * 
	 * @param order
	 *            the {@link Order} to process the payment for. Retrieved from
	 *            the path variable and converted into an {@link Order} instance
	 *            by Spring Data's {@link DomainClassConverter}. Will be
	 *            {@literal null} in case no {@link Order} with the given id
	 *            could be found.
	 * @param number
	 *            the {@link CreditCardNumber} unmarshalled from the request
	 *            payload.
	 * @return
	 */
	@RequestMapping(value = "payment/{orderId}", method = PUT)
	public ResponseEntity<PaymentDTO> submitPayment (@PathVariable("orderId") Long orderId, @RequestBody CreditCardNumber cardNumber) {
		logger.info("Request for order payment : Order Id : {}, Card Number : {}", orderId, cardNumber);
		Order order = orderRepository.findOne(orderId);
		
		if (order == null || order.isPaid()) {
			return new ResponseEntity<PaymentDTO>(HttpStatus.NOT_FOUND);
		}
		
		CreditCardPayment payment = paymentService.pay(order, cardNumber);

		PaymentDTO resource = new PaymentDTO(order.getPrice(), payment.getCreditCard());

		return new ResponseEntity<PaymentDTO>(resource, HttpStatus.CREATED);
	}
	
	/**
	 * Shows the {@link Receipt} for the given order.
	 * 
	 * @param order
	 * @return
	 */
	@RequestMapping(value = "payment/{orderId}", method = GET)
	public ResponseEntity<Receipt> showReceipt(@PathVariable("orderId") Long orderId) {
		logger.info("Request for show recipt : Order Id : {}", orderId);
		Order order = orderRepository.findOne(orderId);
		if (order == null || !order.isPaid() || order.isTaken()) {
			return new ResponseEntity<Receipt>(HttpStatus.NOT_FOUND);
		}

		Payment payment = paymentService.getPaymentFor(order);

		if (payment == null) {
			return new ResponseEntity<Receipt>(HttpStatus.NOT_FOUND);
		}

		return createReceiptResponse(payment.getReceipt());
	}
	
	/**
	 * Takes the {@link Receipt} for the given {@link Order} and thus completes
	 * the process.
	 * 
	 * @param order
	 * @return
	 */
	@RequestMapping(value = "payment/{orderId}", method = DELETE)
	public ResponseEntity<Receipt> takeReceipt(@PathVariable("orderId") Long orderId) {
		logger.info("Request for take recipt : Order Id : {}", orderId);
		Order order = orderRepository.findOne(orderId);
		if (order == null || !order.isPaid()) {
			return new ResponseEntity<Receipt>(HttpStatus.NOT_FOUND);
		}

		return createReceiptResponse(paymentService.takeReceiptFor(order));
	}
	
	/**
	 * Renders the given {@link Receipt} including links to the associated
	 * {@link Order} as well as a self link in case the {@link Receipt} is still
	 * available.
	 * 
	 * @param receipt
	 * @return
	 */
	private ResponseEntity<Receipt> createReceiptResponse(Receipt receipt) {
		return new ResponseEntity<Receipt>(receipt, HttpStatus.OK);
	}
}
