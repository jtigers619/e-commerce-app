package org.javatigers.payment.exception;

import java.util.Objects;

import org.javatigers.order.domain.Order;

/**
 * 
 * @author ad
 *
 */
public class PaymentException extends RuntimeException {

	/**
	 * UUID
	 */
	private static final long serialVersionUID = 7074330730956267856L;
	private final Order order;

	public PaymentException(Order order, String message) {

		super(message);

		Objects.nonNull(order);
		this.order = order;
	}

	public Order getOrder() {
		return order;
	}
}
