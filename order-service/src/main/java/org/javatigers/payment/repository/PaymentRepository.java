package org.javatigers.payment.repository;

import org.javatigers.order.domain.Order;
import org.javatigers.payment.domain.Payment;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Repository interface to manage {@link Payment} instances.
 * 
 * @author ad
 */
public interface PaymentRepository extends JpaRepository<Payment, Long> {

	/**
	 * Returns the payment registered for the given {@link Order}.
	 * 
	 * @param order
	 * @return Payment
	 */
	Payment findByOrder(Order order);
}
