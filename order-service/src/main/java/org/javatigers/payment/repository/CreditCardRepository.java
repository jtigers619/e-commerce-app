package org.javatigers.payment.repository;

import org.javatigers.payment.domain.CreditCard;
import org.javatigers.payment.domain.CreditCardNumber;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Repository to access {@link CreditCard} instances.
 * 
 * @author ad
 */
public interface CreditCardRepository extends JpaRepository<CreditCard, Long> {
	
	/**
	 * Returns the {@link CreditCard} assicaiated with the given
	 * {@link CreditCardNumber}.
	 * 
	 * @param number
	 *            must not be {@literal null}.
	 * @return CreditCard
	 */
	CreditCard findByNumber(CreditCardNumber number);
}
