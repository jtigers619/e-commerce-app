package org.javatigers.order;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Currency;
import java.util.HashSet;

import org.javatigers.order.domain.Item;
import org.javatigers.order.domain.MonetaryAmount;
import org.javatigers.order.domain.Order;
import org.javatigers.order.domain.Order.Status;
import org.javatigers.order.repository.OrderRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.stereotype.Component;

/**
 * 
 * @author ad
 *
 */
@EnableDiscoveryClient
@EnableResourceServer
@EnableJpaRepositories(basePackages = { "org.javatigers.order.domain", "org.javatigers.payment.domain" })
@EnableAsync
@SpringBootApplication(scanBasePackages = { "org.javatigers.order", "org.javatigers.payment" })
public class OrderServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(OrderServiceApplication.class, args);
	}

	@Component
	class OrderCLR implements CommandLineRunner {

		private final OrderRepository orderRepository;

		public OrderCLR(OrderRepository orderRepository) {
			this.orderRepository = orderRepository;
		}

		@Override
		public void run(String... args) throws Exception {
			Item item1 = Item.builder().name("ItemName1").quantity(1)
					.price(new MonetaryAmount(Currency.getInstance("INR"), 88.8)).build();
			Item item2 = Item.builder().name("ItemName2").quantity(1)
					.price(new MonetaryAmount(Currency.getInstance("INR"), 99.9)).build();
			Item item3 = Item.builder().name("ItemName3").quantity(1)
					.price(new MonetaryAmount(Currency.getInstance("INR"), 88.8)).build();
			Item item4 = Item.builder().name("ItemName4").quantity(1)
					.price(new MonetaryAmount(Currency.getInstance("INR"), 99.9)).build();
			Order order1 = Order.builder().orderedDate(LocalDateTime.now()).status(Status.PAYMENT_EXPECTED)
					.items(new HashSet<>(Arrays.asList(item1, item2))).build();
			Order order2 = Order.builder().orderedDate(LocalDateTime.now()).status(Status.PAYMENT_EXPECTED)
					.items(new HashSet<>(Arrays.asList(item3, item4))).build();
			orderRepository.save(Arrays.asList(order1, order2));
			orderRepository.findAll().forEach(System.out::println);
			orderRepository.findAll().stream()
					.forEach(order -> System.out.println("Order Total : " + order.getPrice()));
		}

	}
}
