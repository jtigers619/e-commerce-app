package org.javatigers.order.rest;

import static org.springframework.web.bind.annotation.RequestMethod.DELETE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;
import static org.springframework.web.bind.annotation.RequestMethod.PUT;

import java.io.IOException;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletResponse;

import org.javatigers.order.domain.Order;
import org.javatigers.order.domain.Order.Status;
import org.javatigers.order.engine.InProgressAware;
import org.javatigers.order.repository.OrderRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 
 * @author ad
 *
 */
@CrossOrigin
@RestController
@RequestMapping("/api/v1/")
public class OrderRSV1 {
	private static final Logger logger = LoggerFactory.getLogger(OrderRSV1.class);
	
	private final InProgressAware inProgressAware;
	private final OrderRepository orderRepository;
	
	@Autowired
	public OrderRSV1 (OrderRepository orderRepository, InProgressAware inProgressAware) {
		this.orderRepository = orderRepository;
		this.inProgressAware = inProgressAware;
	}
	
	@RequestMapping(value = "ping", method = GET)
	public String ping () {
		return "<h1>Welcome to Order API!</h1>";
	}
	
	@RequestMapping(value = "orders/{id}", method = GET)
	public Order readOrder (@PathVariable("id") Long id) {
		logger.debug("Request for read order with id : {}", id);
		return orderRepository.findOne(id);
	}
	
	@RequestMapping(value = "orders", method = GET)
	public List<Order> listOrder () {
		logger.debug("Request for list order");
		return orderRepository.findAll();
	}
	
	@RequestMapping(value = "orders/{status}", method = GET)
	public List<Order> listOrderWithStatus (@PathVariable("status") String status) {
		logger.debug("Request for list order with status : {}", status);
		
		return orderRepository.findByStatus(Status.valueOf(status));
	}
	
	@RequestMapping(value = "orders/{id}", method = DELETE)
	public void deleteOrder (@PathVariable("id") Long id) {
		logger.debug("Request for delete order with id : {}", id);
		orderRepository.delete(id);
	}
	
	@RequestMapping(value = "orders", method = POST)
	public Order createOrder (@RequestBody Order order) {
		logger.debug("Request for create order");
		return orderRepository.save(order);
	}
	
	@RequestMapping (value = "orders/{id}", method = PUT)
	public Order updateOrder (@PathVariable("id") Long id, @RequestBody Order order) {
		logger.debug("Request for update order with id : {}", id);
		order.setId(orderRepository.findOne(id).getId());
		return orderRepository.save(order);
	}
	
	@RequestMapping(value = "orders/inProgress", method = GET)
	public Set<Order> readOrdersInProgress () {
		return inProgressAware.getOrders();
	}
	
	@ExceptionHandler(Exception.class)
    void handleExceptions(HttpServletResponse response) throws IOException {
        response.sendError(HttpStatus.INTERNAL_SERVER_ERROR.value(), "Internal server error. We will be addressing this issue soon.");
    }
}
