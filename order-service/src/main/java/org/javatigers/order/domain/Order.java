package org.javatigers.order.domain;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collector;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Version;

import org.javatigers.order.domain.listener.OrderListener;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 
 * @author ad
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EntityListeners(OrderListener.class)
@Entity
@Table(name = "orders")
public class Order implements Serializable {

	/**
	 * UUID
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@Version
	@Column(name = "version")
	private Integer version;

	private Status status;
	
	private LocalDateTime orderedDate;
	
	@Builder.Default
	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
	private Set<Item> items = new HashSet<Item>();

	public Order(Collection<Item> items) {
		this.status = Status.PAYMENT_EXPECTED;
		this.items.addAll(items);
		this.orderedDate = LocalDateTime.now();
	}
	
	public Order(Item... items) {
		this(Arrays.asList(items));
	}
	
	public MonetaryAmount getPrice() {
		return items.stream()
					.map(item -> item.getPrice())
					.collect(sum());
	}
	
	/**
	 * Sum {@link MonetaryAmount}
	 * 
	 * @return
	 */
	public static <T> Collector<MonetaryAmount, List<MonetaryAmount>, MonetaryAmount> sum() {
        return Collector.of(
        		ArrayList::new,
        		(col, value) -> {
        			if (null != value) {
        				col.add(value);
        			}
        		}, 
        		(l1,l2) -> {
        			l1.addAll(l2);
        			return l1;
        		}, 
        		monataryCol -> monataryCol.isEmpty() ? MonetaryAmount.ZERO : monataryCol.stream().reduce(MonetaryAmount.ZERO, MonetaryAmount::add)
        	);
    }

	public void markPaid() {

		if (isPaid()) {
			throw new IllegalStateException("Already paid order cannot be paid again!");
		}

		this.status = Status.PAID;
	}

	public void markInPreparation() {

		if (this.status != Status.PAID) {
			throw new IllegalStateException(String
					.format("Order must be in state payed to start preparation! " + "Current status: %s", this.status));
		}

		this.status = Status.PREPARING;
	}

	public void markPrepared() {

		if (this.status != Status.PREPARING) {
			throw new IllegalStateException(String.format(
					"Cannot mark Order prepared that is currently not " + "preparing! Current status: %s.",
					this.status));
		}

		this.status = Status.READY;
	}

	public boolean isPaid() {
		return !this.status.equals(Status.PAYMENT_EXPECTED);
	}

	public boolean isReady() {
		return this.status.equals(Status.READY);
	}

	public boolean isTaken() {
		return this.status.equals(Status.TAKEN);
	}

	
	public static enum Status {

		/**
		 * Placed, but not payed yet. Still changeable.
		 */
		PAYMENT_EXPECTED,

		/**
		 * {@link Order} was payed. No changes allowed to it anymore.
		 */
		PAID,

		/**
		 * The {@link Order} is currently processed.
		 */
		PREPARING,

		/**
		 * The {@link Order} is ready to be picked up by the customer.
		 */
		READY,

		/**
		 * The {@link Order} was completed.
		 */
		TAKEN;
	}
	
}
