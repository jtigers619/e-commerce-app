package org.javatigers.order.domain.listener;

import javax.persistence.PreRemove;

import org.javatigers.order.domain.Order;
import org.javatigers.order.exception.OrderAlreadyPaidException;

/**
 * Event listener to reject {@code DELETE} requests to Spring REST.
 * 
 * @author ad
 *
 */
public class OrderListener {
	
	@PreRemove
	protected void onBeforeDelete(Object object) {
		if (object instanceof Order) {
			if (Order.class.cast(object).isPaid()) {
				throw new OrderAlreadyPaidException();
			}
		}
		
	}
}
