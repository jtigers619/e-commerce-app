package org.javatigers.order.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Currency;

import javax.persistence.Embeddable;

/**
 * 
 * @author ad
 *
 */
@Embeddable
public class MonetaryAmount implements Serializable {

	/**
	 * UUID
	 */
	private static final long serialVersionUID = 3331494077067593461L;
	
	public static Currency INR = Currency.getInstance("INR");
	public static MonetaryAmount ZERO = new MonetaryAmount();
	private final Currency currency;
	private final BigDecimal value;
	
	public MonetaryAmount(Currency currency, double value) {
		this(currency, new BigDecimal(value));
	}

	private MonetaryAmount(Currency currency, BigDecimal value) {
		this.currency = currency;
		this.value = value;
	}

	protected MonetaryAmount() {
		this(INR, BigDecimal.ZERO);
	}
	
	@Override
	public String toString() {
		return currency + value.toString();
	}

	public MonetaryAmount add(MonetaryAmount other) {

		if (other == null) {
			return this;
		}
		if (!this.currency.equals(other.currency)) {
			throw new RuntimeException ("Currency mismatch.");
		}
		return new MonetaryAmount(this.currency, this.value.add(other.value));
	}

	public Currency getCurrency() {
		return currency;
	}

	public BigDecimal getValue() {
		return value;
	}
}
