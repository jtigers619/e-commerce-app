package org.javatigers.order.exception;

import org.springframework.data.domain.Sort.Order;

/**
 * Exception being thrown in case an {@link Order} has already been paid and a
 * payment is re-attempted.
 * 
 */
public class OrderAlreadyPaidException extends RuntimeException {

	private static final long serialVersionUID = 1L;
}
