package org.javatigers.order.engine;

import java.util.Collections;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.javatigers.order.domain.Order;
import org.javatigers.order.repository.OrderRepository;
import org.javatigers.payment.event.OrderPaidEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

/**
 * Simple {@link ApplicationListener} implementation that listens to
 * {@link OrderPaidEvent}s marking the according {@link Order} as in process,
 * sleeping for 10 seconds and marking the order as processed right after that.
 * 
 * @author ad
 */
@Service
public class OrderEngineService implements ApplicationListener<OrderPaidEvent>, InProgressAware {

	private final OrderRepository repository;
	private final Set<Order> ordersInProgress;
	
	@Autowired
	public OrderEngineService(OrderRepository repository) {
		this.repository = repository;
		this.ordersInProgress = Collections.newSetFromMap(new ConcurrentHashMap<Order, Boolean>());
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public Set<Order> getOrders() {
		return ordersInProgress;
	}

	/**
	 * {@inheritDoc}
	 */
	@Async
	@Override
	public void onApplicationEvent(OrderPaidEvent event) {
		Order order = repository.findOne(event.getOrderId());
		order.markInPreparation();
		order = repository.save(order);

		ordersInProgress.add(order);
		//TODO Notification service
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			throw new RuntimeException(e);
		}

		order.markPrepared();
		repository.save(order);

		ordersInProgress.remove(order);
		
	}

}
