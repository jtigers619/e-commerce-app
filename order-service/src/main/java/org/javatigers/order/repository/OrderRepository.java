package org.javatigers.order.repository;

import java.util.List;

import org.javatigers.order.domain.Order;
import org.javatigers.order.domain.Order.Status;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * 
 * @author ad
 *
 */
@Repository
public interface OrderRepository extends JpaRepository<Order, Long> {
	
	/**
	 * List order by status.
	 * 
	 * @param status
	 * @return List<Order>
	 */
	List<Order> findByStatus(Status status);
}
