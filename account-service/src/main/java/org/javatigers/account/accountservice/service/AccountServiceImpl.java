package org.javatigers.account.accountservice.service;

import org.javatigers.account.accountservice.model.Account;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.stereotype.Service;

import io.reactivex.Observable;

@Service
public class AccountServiceImpl implements AccountService {
	
	private final Logger logger = LoggerFactory.getLogger(getClass());
	
	private OAuth2RestTemplate restTemplate;
	
	@Autowired
	public AccountServiceImpl (OAuth2RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}
	
	@Override
	public Observable<Account> findByName(String accountName) {
		logger.info(String.format("find user by name {}", accountName));
		return Observable.<Account>create (obSub -> {
			try {
				obSub.onNext(restTemplate.getForEntity(String.format("http://oauth-server/uaa/user/%s", accountName), Account.class).getBody());
				obSub.onComplete();
			} catch (Exception e) {
				obSub.onError(e);
			}
		});
	}

	@Override
	public Observable<Account> create(Account user) {
		return Observable.<Account>create(obOnSub ->  {
			try {
				restTemplate.postForObject("http://oauth-server/uaa/user", user, Account.class);
				obOnSub.onNext(restTemplate.getForEntity(String.format("http://oauth-server/uaa/user/%s", user.getUsername()), Account.class).getBody());
				obOnSub.onComplete();
			} catch (Exception e) {
				obOnSub.onError(e);
			}
		});
	}

}
