package org.javatigers.account.accountservice.rest;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

import org.javatigers.account.accountservice.model.Account;
import org.javatigers.account.accountservice.service.AccountService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 
 * @author ad
 *
 */
@RestController
@RequestMapping(value = "/api/v1/")
public class AccountRSV1 {
	
	private final Logger logger = LoggerFactory.getLogger(getClass());
	private final AccountService accountService;
	
	@Autowired
	public AccountRSV1 (AccountService accountService) {
		this.accountService = accountService;
	}
	
	@RequestMapping(value = "account/ping", method = GET)
	public String ping () {
		return "<h1>Welcome to Account API!</h1>";
	}
	
	@RequestMapping (value = "account/{accountName}", method = GET)
	public Account readAccountByName (@PathVariable("accountName") String accountName) {
		logger.info("Read accountBy Name");
		return accountService.findByName(accountName)
					.blockingFirst();
	}
	
	@RequestMapping(value = "account", method = POST)
	public Account createAccount (@RequestBody Account account) {
		logger.info("Create account : {}", account);
		return accountService.create(account)
					.blockingFirst();
	}
}
