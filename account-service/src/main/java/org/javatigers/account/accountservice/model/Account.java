package org.javatigers.account.accountservice.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 
 * @author ad
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Account {
	private Long id;
	private String username;
	private String password;
	private boolean active;
}
