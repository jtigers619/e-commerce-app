package org.javatigers.api.gateway;

import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.csrf.CookieCsrfTokenRepository;

/**
 * 
 * @author ad
 *
 */
//@Configuration
//@EnableOAuth2Sso
public class SSOConfiguration extends WebSecurityConfigurerAdapter {
	@Override	
	public void configure(HttpSecurity http) throws Exception {
		// @formatter:off
		http.cors()
			.and()
			.logout()
			.and()
				.authorizeRequests().antMatchers("/**/*.html", "/login", "/uaa/oauth/token").permitAll()
				.antMatchers(HttpMethod.OPTIONS).permitAll()
				.antMatchers("/*/api/v1/ping").permitAll()
				//.antMatchers("/cart-api/api/v1/cart/123").permitAll()
				.antMatchers("/product-api/api/v1/products/recomendations").permitAll()
				.antMatchers("/*/api/v1/**", "/uaa/user").fullyAuthenticated()
			.and()
				.csrf().csrfTokenRepository(CookieCsrfTokenRepository.withHttpOnlyFalse());
		// @formatter:on
	}
	
}
