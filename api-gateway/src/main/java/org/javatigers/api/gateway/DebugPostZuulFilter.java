package org.javatigers.api.gateway;

import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Component;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;

@Component
public class DebugPostZuulFilter extends ZuulFilter {

	public String filterType() {
		return "post";
	}

	public int filterOrder() {
		return 999;
	}

	public boolean shouldFilter() {
		return true;
	}

	public Object run() {
		RequestContext context = RequestContext.getCurrentContext();
		HttpServletResponse servletResponse = context.getResponse();
		servletResponse.getHeaderNames()
		 .stream()
		 .forEach(System.out::println);
		
		System.out.println("Status : " + servletResponse.getStatus());
		return null;
	}

}
