package org.javatigers.api.gateway;

import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Component;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;

@Component
public class DebugPreZuulFilter extends ZuulFilter {

	@Override
	public boolean shouldFilter() {
		return true;
	}

	@Override
	public Object run() {
		RequestContext context = RequestContext.getCurrentContext();
		HttpServletRequest servletReq = context.getRequest();
		Enumeration<String> headers = servletReq.getHeaderNames();
		while (headers.hasMoreElements()) {
			System.out.println(headers.nextElement());
		}
		System.out.println("Method : " + servletReq.getMethod());
		System.out.println("Url : " + servletReq.getRequestURI());
		return null;
	}

	@Override
	public String filterType() {
		
		return "pre";
	}

	@Override
	public int filterOrder() {
		return 6;
	}

}
