package org.javatigers.api.gateway;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;

/**
 * 
 * @author ad
 *
 */
@Configuration
@EnableResourceServer
public class ResourceServerConfiguration extends ResourceServerConfigurerAdapter {
	
	@Override
	public void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests()
			.antMatchers("/*/api/v1/ping").permitAll()
			.antMatchers(HttpMethod.OPTIONS).permitAll()
			//.antMatchers("/cart-api/api/v1/cart/123").permitAll()
			.antMatchers("/product-api/api/v1/products/recomendations").permitAll()
			.antMatchers("/*/api/v1/**").fullyAuthenticated();
			
	}
	
}
