
import {Inject, Injectable} from '@angular/core';
import {Http, Headers} from '@angular/http';
import {ProductDto} from '../productCatalog/product.dto';
import {Observable} from 'rxjs/Observable';
import {CartItemDto} from './cart.item.dto';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {CartDto} from './cart.dto';
import {AbstractService} from '../abstract.service';
import {AppOauthService} from '../oauth/app.oauth.service';
import {OAuthService} from 'angular-oauth2-oidc';

@Injectable()
export class CartService extends AbstractService {

  private cartUrl: String = this.baseUrl + '/cart-api/api/v1/cart/';

  cartItemOb:  BehaviorSubject<CartItemDto[]> = new BehaviorSubject(null);
  constructor(private oauthService: OAuthService, private http: Http) {
    super();
  }

  addToCart(productDTO: ProductDto): Observable<CartDto> {

    return this.http.put(this.cartUrl + '123', JSON.stringify(this.convertProductToCartItem(productDTO)), {headers: this.getAuthHeader ()})
      .map(response => response.json());
  }

  readCart (): Observable<CartDto> {
    return this.http.get(this.cartUrl + '123', {headers: this.getAuthHeader ()})
      .map(response => response.json());
  }

  private getAuthHeader (): Headers {
    return  new Headers({
        'Authorization': 'Bearer ' + this.oauthService.getAccessToken(),
        'Accept': 'application/json',
        'Content-Type': 'application/json'
    });
  }

  private convertProductToCartItem (productDTO: ProductDto): CartItemDto {
    const cartItem = new CartItemDto();
    cartItem.name = productDTO.name;
    cartItem.currency = productDTO.currency;
    cartItem.price = productDTO.price;
    return cartItem;
  }

}
