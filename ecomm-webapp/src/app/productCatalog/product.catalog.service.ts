import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import {ProductDto} from './product.dto';
import {AbstractService} from '../abstract.service';

@Injectable()
export class ProductCatalogService extends AbstractService {

  private productUrl: String = this.baseUrl + '/product-api/api/v1/products/';
  constructor(private http: Http) {
    super();
  }

  getProductRecomendations (): Observable<ProductDto[]> {
    return this.http.get(this.productUrl + 'recomendations')
      .map(response => response.json());
  }
}
