import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {BrowserXhr, HttpModule} from '@angular/http';

import {ProductCatalogModule} from './productCatalog/product.catalog.module';
import { AppComponent } from './app.component';
import {RouterModule, Routes} from '@angular/router';
import {CartModule} from './cart/cart.module';
import {LoginComponent} from './login/login.component';
import {ProductCatalogComponent} from './productCatalog/product.catalog.component';
import {AppOauthService} from './oauth/app.oauth.service';
import {OAuthModule} from 'angular-oauth2-oidc';
import {NavbarModule} from './nav-bar/navbar.module';
import {HeaderComponent} from './header/header.component';
import {FooterComponent} from './footer/footer.component';

const appRoutes: Routes = [
  { path: '', component: ProductCatalogComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent
    ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    OAuthModule.forRoot(),
    NavbarModule,
    ProductCatalogModule,
    CartModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [ AppOauthService ],   // <-- Provider for OAuthService,
  bootstrap: [AppComponent]
})
export class AppModule { }
