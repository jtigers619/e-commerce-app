import {Injectable} from '@angular/core';
import {Http, Headers} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {UserDto} from '../login/user.dto';
import 'rxjs/Rx';
import {Router} from '@angular/router';
import {JwksValidationHandler, OAuthService} from 'angular-oauth2-oidc';
import {authConfig} from './auth.config';

@Injectable()
export class AppOauthService {

  constructor(private router: Router, private http: Http, private oauthService: OAuthService) {
    this.configureWithNewConfigApi();
  }

  private configureWithNewConfigApi() {
    this.oauthService.configure(authConfig);
    this.oauthService.tokenValidationHandler = new JwksValidationHandler();
    this.oauthService.oidc = false;
    this.oauthService.setStorage(sessionStorage);
    this.oauthService.tryLogin({});
  }

  obtainAccessToken() {
    this.oauthService.initImplicitFlow();
  }

  public getToken() {
    return this.oauthService.getAccessToken();
  }

  public getUserProfile(): any {
    return this.oauthService.loadUserProfile();
  }

  getUser(): Observable<UserDto> {
    console.log('Access Token : ' + this.getAuthHeader().get('Authorization'));
    return this.http.get('http://localhost:18080/uaa/user', {headers: this.getAuthHeader()})
      .map(response => response.json());
  }

  isLoggedIn() {
    if (this.oauthService.getAccessToken() === null) {
      return false;
    }
    return true;
  }

  logout() {
    this.oauthService.logOut();
    location.reload();
  }

  private getAuthHeader(): Headers {
    return new Headers({
      'Authorization': 'Bearer ' + this.oauthService.getAccessToken(),
      'Accept': 'application/json'
    });
  }
}
