import { AuthConfig } from 'angular-oauth2-oidc';

export const authConfig: AuthConfig = {

  // Url of the Identity Provider
  loginUrl: 'http://localhost:18083/uaa/oauth/authorize',
  // URL of the SPA to redirect the user to after login
  redirectUri: 'http://localhost:4200/',

  // The SPA's id. The SPA is registerd with this id at the auth-server
  clientId: 'acme',
  // set the scope for the permissions the client should request
  // The first three are defined by OIDC. The 4th is a usecase-specific one
  scope: 'openid',
  showDebugInformation: true,
  userinfoEndpoint: 'http://localhost:18080/uaa/user',
  tokenEndpoint: 'http://localhost:18083/uaa/oauth/token',
  requireHttps: false
};
