import {Headers} from '@angular/http';

export abstract class AbstractService {
  protected baseUrl: String = 'http://localhost:18080';

  protected getHeaders() {
    const headers = new Headers();
    headers.append('Accept', 'application/json');
    headers.append('Content-Type', 'application/json');
    return headers;
  }
}
