/**
 * Created by ad on 4/28/2017.
 */
import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {NavbarComponent} from './navbar.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';
import {LoginComponent} from '../login/login.component';
@NgModule({
 declarations: [
   NavbarComponent,
   LoginComponent,
 ],
  imports: [
    CommonModule,
    FormsModule,
    HttpModule,
    ReactiveFormsModule
  ],
  exports: [
    NavbarComponent,
    LoginComponent
  ]
})
export class NavbarModule {

}
