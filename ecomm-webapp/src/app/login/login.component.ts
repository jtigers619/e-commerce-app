
import {Component, OnInit} from '@angular/core';

import {UserDto} from './user.dto';
import {AppOauthService} from '../oauth/app.oauth.service';


@Component ({
  selector: 'app-login',
  templateUrl: './login.component.html',
  providers: []
})
export class LoginComponent implements OnInit {

  public isLoggedIn = false;
  public userDTO: UserDto;

  constructor(private appOAuthService: AppOauthService) {}

  ngOnInit () {
    this.isLoggedIn = this.appOAuthService.isLoggedIn();
    if (this.isLoggedIn) {
      // this.appOAuthService.getUser().subscribe((user) => console.log('User' + user),
        // (err) => console.log('Error : ' + err));
      // console.log('Access Token : ' + this.appOAuthService.getToken());
      this.appOAuthService.getUserProfile().then(res => {
          this.userDTO = res;
          console.log('User DTO : ' + this.userDTO.username);
        });

    }
  }

  public login () {
    this.appOAuthService.obtainAccessToken();
  }

  public logout () {
    this.appOAuthService.logout();
  }
}
